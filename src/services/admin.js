import http from '../config/http'

//GET
const getVapes = () => http.get('/vapes')

// DElELE
const deleteVape = (id) => http.delete(`/vapes/${id}`)

// CREATE
const createVape = (data, config = {}) => http.post(`/vapes`, data, config)
// UPDATE
const updateVape = (id, data) => http.patch(`/vapes/${id}`, data)


export {
    getVapes,
    deleteVape,
    createVape,
    updateVape
}