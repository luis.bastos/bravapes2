
import React from 'react'
import styled from 'styled-components'
import Carousel from 'react-bootstrap/Carousel'
import { Row, Col, Button } from 'react-bootstrap'
import BgBanner from '../../assets/images/fumaça.jpg'
import Img1 from '../../assets/images/ooooo.jpg'
import Img2 from '../../assets/images/qqqqqqqq.png'
import Img3 from '../../assets/images/vvvv.png'
import Img4 from '../../assets/images/zzzz.png'

const BannerHome = () => {
    return (
        <Banner>
            <div className="bg">
                <BannerItem>
                    <Carousel controls={false}>
                        <Carousel.Item>
                            <Row className="mt-5 py-3 justify-content-center align-items-center">
                                <Col md={3} sm={3}>
                                    <img src={Img1} className="img-fluid" alt="" />
                                </Col>
                                <Col md={3} sm={3} className="mb-4">
                                    <div className="tag">Texto</div>
                                    <h2 className="mb-4 title">Texto</h2>
                                    <div className="mb-4 desc">Texto</div>
                                    <Button variant="danger">Comprar</Button>
                                </Col>
                            </Row>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Row className="mt-5 py-3 justify-content-center align-items-center">
                                <Col md={3} sm={3}>
                                    <img src={Img2} className="img-fluid" alt="" />
                                </Col>
                                <Col md={3} sm={3} className="mb-4">
                                    <div className="tag">Texto</div>
                                    <h2 className="mb-4 title">Texto </h2>
                                    <div className="mb-4 desc">Texto</div>
                                    <Button variant="danger">Comprar</Button>
                                </Col>
                            </Row>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Row className="mt-5 py-3 justify-content-center align-items-center">
                                <Col md={3} sm={3}>
                                    <img src={Img3} className="img-fluid" alt="" />
                                </Col>
                                <Col md={3} sm={3} className="mb-4">
                                    <div className="tag">Texto</div>
                                    <h2 className="mb-4 title">Texto </h2>
                                    <div className="mb-4 desc">Texto</div>
                                    <Button variant="danger">Comprar</Button>
                                </Col>
                            </Row>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Row className="mt-5 py-3 justify-content-center align-items-center">
                                <Col md={3} sm={3}>
                                    <img src={Img4} className="img-fluid" alt="" />
                                </Col>
                                <Col md={3} sm={3} className="mb-4">
                                    <div className="tag">Texto</div>
                                    <h2 className="mb-4 title">Texto </h2>
                                    <div className="mb-4 desc">Texto</div>
                                    <Button variant="danger">Comprar</Button>
                                </Col>
                            </Row>
                        </Carousel.Item>
                    </Carousel>
                </BannerItem>
            </div>
        </Banner>
    )
}

export default BannerHome


const Banner = styled.div`
    display: ${props => props.hidden === true ? 'none' : 'block'};
    height: 700px;
    width: 100%;
    background-image: url(${BgBanner});
    background-size: 100% 100%;
    overflow: hidden;

    .bg{
        background: #0003;
        height: 500px;
    }
`

const BannerItem = styled.div`
    color: #fff
`