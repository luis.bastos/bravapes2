import React, { useState, useEffect } from 'react'
import { Button, Table } from 'react-bootstrap'
import styled from 'styled-components'
import Swal from 'sweetalert2'
import { getVapes, deleteVape } from '../../services/admin'
import { FaRegEdit, FaTrashAlt, FaCheckCircle, FaTimesCircle } from 'react-icons/fa'


const ListVapes = (props) => {
    const [vapes, setVapes] = useState([])
    const [isUpdate, setUpdate] = useState(false)

    useEffect(() => {
        setUpdate(false)
        let get = async () => {
            try {
                const prd = await getVapes();
                setVapes(prd.data)
            } catch (error) {
                console.log(error)
            }
        }
        if (!isUpdate) {
            get();
        }
        //clear
        return () => get = () => { };
    }, [isUpdate])

    const _deleteCategory = async (obj) => {
        const message = (type, message) => Swal.fire({
            position: 'top-end',
            icon: type || 'success',
            title: message || `Vape excluído com sucesso.`,
            showConfirmButton: false,
            timer: 2500
        })

        Swal.fire({
            title: `Deseja excluir o vape ${obj.title} `,
            showCancelButton: true,
            confirmButtonText: `Sim`,
            cancelButtonText: `Não`,
        }).then((result) => {
            if (result.isConfirmed) {
                deleteVape(obj._id)
                    .then(() => {
                        setUpdate(true)
                        message('success', `Vape ${obj.title} excluído com sucesso.`)
                    })
                    .catch(() => message('danger', `Erro ao excluir o vape`))
            }
        })
    }

    return (
        <>
            <NewTable striped hover size="sm">
                <thead>
                    <tr>
                        <THeadItem>Marca</THeadItem>
                        <THeadItem>Modelo</THeadItem>
                        <THeadItem>Voltagem</THeadItem>
                        <THeadItem>Estado</THeadItem>
                        <THeadItem>Cor</THeadItem>
                        <THeadItem></THeadItem>
                    </tr>
                </thead>
                <tbody>
                    {vapes.map((prd, i) => (
                        <tr key={i}>
                            <TbodyItem>{prd.brand}</TbodyItem>
                            <TbodyItem>{prd.vapemodel}</TbodyItem>
                            <TbodyItem>{prd.wattz}</TbodyItem>
                            <TbodyItem>{prd.vapestatus}</TbodyItem>
                            <TbodyItem>{prd.color}</TbodyItem>
                            <TbodyItem>
                                <ActionButton onClick={() => props.updateVape(prd)} variant="link" size="sm">
                                    <FaRegEdit />                                </ActionButton>
                                <ActionButton onClick={() => _deleteCategory(prd)} variant="link" size="sm">
                                    <FaTrashAlt />
                                </ActionButton>
                            </TbodyItem>
                        </tr>
                    ))}

                </tbody>
            </NewTable>
        </>
    )
}

export default ListVapes


const NewTable = styled(Table)`
    font-size: 14px
`

const THeadItem = styled.th`
    background: #666;
    color:#eee;
`
const TbodyItem = styled.td`
    :nth-child(1){  width: 10%; }
    :nth-child(2){  width: 10%; }
    :nth-child(3){  width: 20%; }
    :nth-child(4){  width: 10%; }
    :nth-child(5){  width: 10%; }
    :nth-child(6){  width: 10%; }
    :nth-child(7){  width: 10%; }
    :nth-child(8){  width: 20%; }
`
const ActionButton = styled(Button)`
    padding:2px 4px;
    font-weight:500;
    font-size: 16px;

    :hover {
        opacity:0.4;
        color: red
    }
`
