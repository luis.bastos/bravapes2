import React, { useState, useEffect } from 'react'
import { Button, ButtonGroup, Form, ProgressBar, ToggleButton } from 'react-bootstrap'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import { getVapes, createVape, updateVape } from '../../services/admin'
import { FaCheck, FaTimes } from 'react-icons/fa'
import styled from 'styled-components'
import IntlCurrencyInput from 'react-intl-currency-input'
import history from '../../config/history'
import currencyConfig from '../../config/currency'

const FormVapes = (props) => {

    const [formVape, setFormVape] = useState({
        ...props.update,
        vapes: props.update?.vapes?._id || undefined
    })
    const [vapes, setVapes ] = useState([])
    const [progress, setProgress] = useState(0)
    const [updatePhoto, setUpdatePhoto] = useState(false)


    useEffect(() => {
        let get = async () => {
            const c = await getVapes();
            setVapes(c.data);
        }
        get();
        //clear
        return () => get = () => { };
    }, [])

    const isUpdate = Object.keys(props.update).length > 0

    const typeReq = (data) => isUpdate ? updateVape(props.update._id, data) : createVape(data)

    const handleChange = (attr) => {
        const { value, name, checked } = attr.target
        const isCheck = name === 'brandd' || name === 'vapemodeel'



        if (name === 'photo') {
            setFormVape({
                ...formVape,
                'photo': attr.target.files[0]
            })
        } else {

            setFormVape({
                ...formVape,
                [name]: isCheck ? checked : value
            })
        }
        return;
    }

    const submitVape = async () => {

        const message = (type, message) => Swal.fire({
            position: 'top-end',
            icon: type || 'success',
            title: message,
            showConfirmButton: false,
            timer: 2500
        })
        // conversao dos dados paa formData
        let data = new FormData()


        Object.keys(formVape)
            .forEach(key => data.append(key, formVape[key]))

        const config = {
            onUploadProgress: function (progressEvent) {
                let successPercent = Math.round(progressEvent.loaded * 100 / progressEvent.total)
                setProgress(successPercent)
            },
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }

        typeReq(data, config)
            .then((res) => {
                clearForm()
                message('success', `Vape Cadastrado com sucesso.`)
                history.push('/admin/vapes', { update: true })
            })
            .catch((err) => message('error', `Erro ao cadastrar o vape.`))
    }

    const clearForm = () => {
        setUpdatePhoto(true)
        setFormVape({
            brand: "",
            vapemodel: "",
            wattz: "",
            vapestatus: "",
            color: "",
            photo: ""
        })
    }

    const isNotValid = () => {
        return Object.keys(formVape).some(k => typeof formVape[k] === "string" && formVape[k] === "")
    }

    const removePhoto = () => {
        setUpdatePhoto(true)
        setFormVape({
            ...formVape,
            photo: ""
        })
    }



    return (
        <>
            <Form.Group >
                <Form.Control type="text" onChange={handleChange} name="brand" value={formVape.brand || ""} placeholder="Marca do Vapee" />
            </Form.Group>
            <Form.Group >
                <Form.Control type="text" onChange={handleChange} name="vapemodel" value={formVape.vapemodel || ""} placeholder="Modelo do Vapee" />
            </Form.Group>
            <Form.Group >
                <Form.Control type="text" onChange={handleChange} name="wattz" value={formVape.wattz || ""} placeholder="Voltagem do Vapee" />
            </Form.Group>
            <Form.Group >
                <Form.Control type="text" onChange={handleChange} name="vapestatus" value={formVape.vapestatus || ""} placeholder="Estado do Vapee" />
            </Form.Group>
            <Form.Group >
                <Form.Control type="text" onChange={handleChange} name="color" value={formVape.color || ""} placeholder="Cor do Vapee" />
            </Form.Group>
            <hr />
            <Form.Group >
                {isUpdate && !updatePhoto ? (
                    <Thumb>
                        <img src={formVape.photo} alt="photox" />
                        <span onClick={removePhoto}>Remover</span>
                    </Thumb>
                ) : (
                        <input name="photo" type="file" onChange={handleChange} />
                    )}
            </Form.Group>
            <Form.Group >
                <Button variant="primary" disabled={isNotValid()} onClick={submitVape}>
                    {isUpdate ? "Atualizar" : "Cadastrar"}
                </Button>
            </Form.Group>
            <br />
            {progress > 0 ? <ProgressBar striped variant="success" now={progress} /> : ""}
            <br />

        </>
    )

}

export default FormVapes



const Thumb = styled.div`

    display: flex;
    flex-direction: column;

    img{
        max-width: 200px;
        max-height: 200px;
    }

    span{
        cursor: pointer;
        color: #ccc;
        &:hover{
            color: red
        }
    }


`
