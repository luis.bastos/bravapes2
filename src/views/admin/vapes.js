import React, { useEffect, useState } from 'react'
import ListVapes from '../../components/vape/list'
import FormVapes from '../../components/vape/form'
import { Button } from 'react-bootstrap'
import styled from 'styled-components'
import { useLocation } from 'react-router-dom'

export default () => {
    const [isForm, setForm] = useState(false)
    const [update, setUpdate] = useState({})
    const location = useLocation();

    const updateVape = (prd) => {
        setUpdate(prd)
        setForm(true)
    }

    useEffect(() => {
        if (location.state?.update) {
            setForm(false)
        }
    }, [location])

    return (
        <Products>
            <Button size="sm" variant="success" onClick={() => setForm(!isForm)}>
                {isForm ? "Lista" : "Novo"}
            </Button>
            <hr />
            { isForm
                ? <FormVapes update={update} />
                : <ListVapes updateVape={updateVape} />
            }
        </Products>
    )
}

const Products = styled.div`
 
`