import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'
export default () => {
    return (
        <Contact>
            <TitlePage title="Contato" sub="Veja Nossos Contatos" />
            <Container>
                    <Row>
                        <BoxItem>Insira o contato aqui </BoxItem>
                    </Row>
            </Container>
        </Contact>
    )
}


const Contact = styled.div`
    display:block;
    height: 800px;
`

const Info = styled(Col)`
    background: red;
    width: 100%;
    height: 300px
`
const Form = styled(Col)`
    background: blue;
    width: 100%;
    height: 300px
`
const BoxItem = styled(Col)`
    background: red;
    height: 200px;
    margin: 10px;
    width: 20%;
`





