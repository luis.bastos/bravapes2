import React, { useState, useEffect } from 'react'
import { Container, Tab, Tabs } from 'react-bootstrap'
import styled from 'styled-components'

import TitlePage from '../../../components/titlePage'
import { getVapes } from '../../../services/admin'
import { useDispatch, useSelector} from 'react-redux'
export default () => {
    const dispatch = useDispatch()

    const categories = useSelector(state => state.vapes)
    console.log('categories', categories)
    const [vapes, setVapes] = useState([])

   
   
    useEffect(() => {

        (() => {
            const p = getVapes();
            setVapes(p.data);
        })()

        //clear
        return () => () => { };
    }, [dispatch])
   
   
   
    const mountVapes = (cat) => {

        const prods = vapes.filter(item => item._id === cat._id)
        console.log(prods)
        return (
            <GridCards>

                {prods.length === 0
                    ? <div>Sem vapes</div>
                    : (
                        prods.map((prd, i) => (
                            <li> { prd.title}</li>
                        ))
                    )
                }

            </GridCards >
        )
    }

    return (
        <Product>
            <TitlePage title="Vapes" sub="Conheça nossa Lista de Vapes" />

            <Container>
                <TabBox defaultActiveKey={1} className="vapes">
                    {vapes.map((cat, i) => (
                        <Tab eventKey={i} key={i} title={cat.name}>
                            {mountVapes(cat)}
                        </Tab>
                    ))}
                </TabBox>
            </Container>
        </Product>
    )
}


const Product = styled.div`
    display:block;
    height: 700px;

    .vapes{
        margin-top: 20px;
    }
    .tab-content{
    }
    .tab-pane{
        background: pink;
        display: flex;
        div{
            display: grid;
            grid-gap: 10px;
            /* grid-template-columns: repeat(auto-fit, 250px); */
            width:100%;
        }
    }
`

const TabBox = styled(Tabs)`
    background: white;
`
const GridCards = styled.div`
    
`
