import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'


export default () => {
    return (
        <About>
            <TitlePage title="Sobre" sub="Melhores Vapers" />


            <Collaborators>
                <Container>
                    <Row>
                        <BoxItem>Vapers </BoxItem>
                    </Row>
                </Container>

            </Collaborators>
        </About>
    )
}


const About = styled.div`
    display:block;
`

const Collaborators = styled.div`

    height: 600px;
    width: 100%;
    padding: 20px 0;
`
const BoxItem = styled(Col)`
    background: red;
    height: 200px;
    margin: 10px;
    width: 20%;
`

